# Drones Technology #

This repository contains example about drone technology, Drones is a major new technology that is designed to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

### What is this repository for? ###

* Version: 0.0.1-SNAPSHOT

* Technologies: Spring boot, Spring AOP and H2 in memory DB 

### How do I get set up? ###

## Clone Repo

    git clone https://Adelessam@bitbucket.org/Adelessam/drone-technology.git  
    cd drone-technology/ 
    
## Run

    mvn spring-boot:run
    
    
## REST APIs

    The rest APIs of Drone app is described below.

### Request to retrieve all Drones

`GET /api/drones`  

### Request to register new Drone

`POST /api/drones` 
        
        {
            "serialNumber":"XXXX",  
            "model": "lightWeight",
            "battery":"100"
        }
    
### Request to load Drone With medications

`PATCH /api/drones/{serialNumber}` 
        
        [
          {
                "name":"panadolExtra",
                "weight":100,
                "code":"1234",
                "imageUrl":"www.google.com"
            }
        ]
        

### Request to check loaded medications For Drone

`GET /api/drones/{serialNumber}` 


### Request to check battery level for Drone

`GET /api/drones/battery/{serialNumber}` 


### Request to check Drone history

`GET /api/drones/audit/{serialNumber}`

## NOTE: YOU CAN FIND POSTMAN COLLECTION ATTACHED TO REPO









