package com.musala.drones.utilities.audit;

import com.musala.drones.domain.audit.DroneHistory;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.repository.DroneAuditRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;


@Aspect
@Component
public class LogAspect {

	private  ThreadLocal action = new ThreadLocal();

	@Autowired
	private DroneAuditRepository droneAuditRepository;

	@Around("@annotation(LoggableAction) && within(com.musala..*)")
	public Object aroundLoggableAction(ProceedingJoinPoint jp) throws Throwable {
		LoggableAction annotation = ((MethodSignature) jp.getSignature()).getMethod()
				.getAnnotation(LoggableAction.class);
		action.set(annotation.action());
		return execute(jp, annotation.layer());
	}

	public Object execute(ProceedingJoinPoint jp, String layer) throws Throwable {
		long startTime = System.currentTimeMillis();
		Signature methodSignature = jp.getSignature();
		LogRecord logRecord = new LogRecord((String)action.get(),
				layer + "." + methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName());
		logRecord.setSerialNumber(getSerialNumber(jp.getArgs()));
		logRecord.setActionDate(new Date().toString());
		try {
			return jp.proceed();
		} catch (Throwable e) {
			logRecord.setErrorMessage(e.getMessage());
			throw e;
		} finally {
			logRecord.setResponseTime(System.currentTimeMillis() - startTime);
			saveDroneAudit(logRecord);
			System.out.println(logRecord.toString());

		}
	}

	private void saveDroneAudit(LogRecord logRecord) {
		DroneHistory droneHistory=new DroneHistory();
		droneHistory.setSerialNumber(logRecord.getSerialNumber());
		droneHistory.setLogHistory(logRecord.toString());
		droneAuditRepository.save(droneHistory);
	}

	private String getSerialNumber(Object[] args) {
		for (Object arg : args) {
			if (arg instanceof DroneDTO) {
				DroneDTO droneDTO = (DroneDTO) arg;
				if(droneDTO!=null){
					return droneDTO.getSerialNumber();
				}
			}else if(arg instanceof String){
					return arg.toString();
			}
		}
		return null;
	}

}
