package com.musala.drones.utilities.audit;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LogRecord {

	private String serialNumber;
	private String action;
	private String actionDate;
	private String layer;
	private long responseTime;
	private String errorMessage;

	public LogRecord(String action, String layer) {
		this.action = action;
		this.layer = layer;
	}
}
