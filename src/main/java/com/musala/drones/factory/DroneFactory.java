package com.musala.drones.factory;

import com.musala.drones.dto.enums.UseCase;
import com.musala.drones.service.IDroneService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DroneFactory {

    private IDroneService droneMedicationsService;

    public DroneFactory(@Qualifier("droneMedicationsService") IDroneService droneMedicationsService){
        this.droneMedicationsService=droneMedicationsService;
    }

    public IDroneService getClient(String useCase) {

        if (UseCase.MEDICATIONS_LOAD.getName().equalsIgnoreCase(useCase)) {
            return droneMedicationsService;
        }
        return null;
    }
}
