package com.musala.drones.service;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Medication;
import com.musala.drones.domain.audit.DroneHistory;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.DroneHistoryDTO;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.dto.enums.Model;
import com.musala.drones.dto.enums.State;
import com.musala.drones.exception.DroneExceptions;
import com.musala.drones.repository.DroneAuditRepository;
import com.musala.drones.repository.DroneRepository;
import com.musala.drones.utilities.ObjectMapperUtils;
import com.musala.drones.utilities.audit.LoggableAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Qualifier("droneMedicationsService")
@Service
@Component
public class DroneServiceImp implements IDroneService {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private DroneAuditRepository droneAuditRepository;

    @Override
    @LoggableAction(action = "List All Drones", layer = "Service", method = "getAllDrones")
    public List<DroneDTO> getAllDrones() {
        return ObjectMapperUtils.mapAll(droneRepository.findAll(), DroneDTO.class);
    }

    @Override
    @LoggableAction(action = "Registration", layer = "Service",method = "registerDrone")
    public DroneDTO registerDrone(DroneDTO droneDTO) throws DroneExceptions.CreateDroneExceptions {
        Optional<Drone> droneExist = droneRepository.findById(droneDTO.getSerialNumber());
        if(droneExist.isPresent())
            throw new DroneExceptions.CreateDroneExceptions("Drone already exist");
        Drone drone = ObjectMapperUtils.map(droneDTO, Drone.class);
        drone.setState(State.IDLE.getName());
        return ObjectMapperUtils.map(droneRepository.save(drone), DroneDTO.class);
    }

    @Override
    @LoggableAction(action = "Load Drone with Medications Items", layer = "Service",method = "loadDroneWithMedications")
    public void loadDroneWithMedications(String serialNumber, List<MedicationDTO> medicationItems) throws DroneExceptions {
        Optional<Drone> drone = droneRepository.findById(serialNumber);
        List<Medication> medications=new ArrayList<>();

        if (!drone.isPresent())
            throw new DroneExceptions.DroneNotFoundException("No drone found with serial number " + serialNumber);

        Drone loadedDrone=drone.get();
        if (loadedDrone.getState() != State.IDLE.getName() && loadedDrone.getState() != State.LOADING.getName())
            throw new DroneExceptions.DroneNotValidException("Drone unable to be loaded, its on process: "+ loadedDrone.getState());

        if(loadedDrone.getBattery()<25)
            throw new DroneExceptions.DroneNotValidException("Drone unable to be loaded, battery is low : "+ loadedDrone.getBattery()+"%");

        boolean  isDroneHaveSpace = checkDroneSpace(loadedDrone, medicationItems);
        if(isDroneHaveSpace){
            for (MedicationDTO medicationDTO:medicationItems) {
                Medication medication=ObjectMapperUtils.map(medicationDTO,Medication.class);
                medication.setDrone(loadedDrone);
                medications.add(medication);
            }
        }else
            throw new DroneExceptions.DroneExceedLimitException("Drone exceed weight limit");

        loadedDrone.setMedications(medications);
        loadedDrone.setState(loadedDrone.getWeight()==500?State.LOADED.getName():State.LOADING.getName());
        droneRepository.save(loadedDrone);
    }

    private boolean checkDroneSpace(Drone drone, List<MedicationDTO> medicationItems) {
        double sumOfNewMedicationsWeight=0,sumOfLoadedMedicationsBefore = 0;
        if (medicationItems != null && !medicationItems.isEmpty()) {
            sumOfNewMedicationsWeight = medicationItems.stream().mapToDouble(MedicationDTO::getWeight).sum();
            if (sumOfNewMedicationsWeight > 500 || sumOfNewMedicationsWeight == 0)
                return false;
        }
       if (drone.getState() == State.LOADING.getName()) {
            List<Medication> medications = drone.getMedications();
           sumOfLoadedMedicationsBefore = medications.stream().mapToDouble(Medication::getWeight).sum();
            if (sumOfNewMedicationsWeight + sumOfLoadedMedicationsBefore > 500)
                return false;
        }
        drone.setWeight(sumOfNewMedicationsWeight+sumOfLoadedMedicationsBefore);
        return true;
    }


    @Override
    @LoggableAction(action = "Check Loaded Medications For Drone", layer = "Service",method = "checkLoadedMedicationsForDrone")
    public DroneDTO checkLoadedMedicationsForDrone(String serialNumber) throws DroneExceptions.DroneNotFoundException {
        Optional<Drone> drone=droneRepository.findById(serialNumber);
        if(!drone.isPresent())
            throw new DroneExceptions.DroneNotFoundException("No drone found with serial number: "+ serialNumber);
        return ObjectMapperUtils.map(drone.get(),DroneDTO.class);
    }

    @Override
    @LoggableAction(action = "Check Drone Battery", layer = "Service",method = "checkDroneBattery")
    public Integer checkDroneBattery(String serialNumber) throws DroneExceptions.DroneNotFoundException {
        Optional<Drone> drone=droneRepository.findById(serialNumber);
        if(!drone.isPresent())
            throw new DroneExceptions.DroneNotFoundException("No drone found with serial number: "+ serialNumber);
        return drone.get().getBattery();
    }

    @Override
    public List<DroneHistoryDTO> checkDroneHistory(String serialNumber) throws DroneExceptions.DroneNotFoundException {
        List<DroneHistory> droneHistory=droneAuditRepository.findBySerialNumber(serialNumber);
        if(droneHistory==null||droneHistory.isEmpty())
            throw new DroneExceptions.DroneNotFoundException("No drone found with serial number: "+ serialNumber);
        return ObjectMapperUtils.mapAll(droneHistory, DroneHistoryDTO.class);
    }

    @Override
    @LoggableAction(action = "Check Available Drones For Loading", layer = "Service",method = "checkAvailableDronesForLoading")
    public List<DroneDTO> checkAvailableDronesForLoading() {
        return null;
    }

}
