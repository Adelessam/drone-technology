package com.musala.drones.service;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.audit.DroneHistory;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.DroneHistoryDTO;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.exception.DroneExceptions;
import com.musala.drones.utilities.audit.LoggableAction;
import org.aspectj.lang.annotation.Aspect;

import java.util.List;

public interface IDroneService {

    List<DroneDTO> getAllDrones() ;

    DroneDTO registerDrone(DroneDTO drone) throws DroneExceptions.CreateDroneExceptions;

    void loadDroneWithMedications(String serialNumber, List<MedicationDTO> medicationItems) throws DroneExceptions;

    DroneDTO checkLoadedMedicationsForDrone(String serialNumber) throws DroneExceptions.DroneNotFoundException;

    List<DroneDTO> checkAvailableDronesForLoading();

    Integer checkDroneBattery(String serialNumber) throws DroneExceptions.DroneNotFoundException;

    List<DroneHistoryDTO> checkDroneHistory(String serialNumber) throws DroneExceptions.DroneNotFoundException;



}
