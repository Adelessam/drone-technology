package com.musala.drones.dto.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum State implements Serializable {

    IDLE("idle"),
    LOADING("loading"),
    LOADED("loaded"),
    DELIVERING("delivering"),
    DELIVERED("delivered"),
    RETURNING("returning");

    private final String name;

    State(String name) {
        this.name=name;
    }

    @JsonValue
    public String getName() {
        return name;
    }

}
