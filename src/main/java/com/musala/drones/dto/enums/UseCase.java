package com.musala.drones.dto.enums;

public enum UseCase {

    MEDICATIONS_LOAD("medicationLoad");

    private final String name;

    UseCase(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
}
