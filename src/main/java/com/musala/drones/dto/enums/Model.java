package com.musala.drones.dto.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum Model implements Serializable {

    LIGHT_WEIGHT("lightWeight"),
    MIDDLE_WEIGHT("middleWeight"),
    CRUISER_WEIGHT("cruiserWeight"),
    HEAVY_WEIGHT("heavyWeight");

    private final String modelName;

    Model(String modelName) {
        this.modelName = modelName;
    }

    @JsonValue
    public String getModelName() {
        return modelName;
    }
}
