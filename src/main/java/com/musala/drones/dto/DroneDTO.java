package com.musala.drones.dto;

import com.musala.drones.domain.Medication;
import com.musala.drones.dto.enums.Model;
import com.musala.drones.dto.enums.State;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneDTO implements Serializable {

    @Size(max=100,min = 0,message ="Serial number shouldn't exceed 100 character" )
    private String serialNumber;

    private String model;

    @Max(500)
    private double weight;

    private String battery;

    private String state;

    private List<MedicationDTO> medications;
}
