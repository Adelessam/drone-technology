package com.musala.drones.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicationDTO implements Serializable {

    @Pattern(regexp = "[A-Za-z0-9-_]*", message = "Name allowed only letters, numbers, ‘-‘, ‘_’")
    private String name;

    @Max(500)
    private double weight;

    @Pattern(regexp = "[A-Z0-9_]*", message = "Code allowed only upper case letters, underscore and numbers")
    private String code;

    private String imageUrl;

}
