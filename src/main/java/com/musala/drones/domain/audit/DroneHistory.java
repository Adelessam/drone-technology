package com.musala.drones.domain.audit;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "DRONE_AUDIT")
@Data
public class DroneHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serial_number", length = 100)
    private String serialNumber;

    @Column(name = "logHistory")
    private String logHistory;
}
