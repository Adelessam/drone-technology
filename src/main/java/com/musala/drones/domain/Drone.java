package com.musala.drones.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DRONE")
@Data
@ToString
public class Drone {

    @Id
    @Column(name = "serial_number", length = 100, unique = true)
    private String serialNumber;

    @Column(name = "model")
    private String model;

    @Column(name = "weight")
    private double weight;

    @Column(name = "battery")
    private int battery;

    @Column(name = "state")
    private String state;

    @OneToMany(mappedBy = "drone", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Medication> medications;

}
