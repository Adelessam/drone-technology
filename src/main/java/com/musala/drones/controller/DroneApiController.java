package com.musala.drones.controller;

import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.DroneHistoryDTO;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.exception.DroneExceptions;
import com.musala.drones.factory.DroneFactory;
import com.musala.drones.service.IDroneService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/drones")
public class DroneApiController {

    private final DroneFactory droneFactory;

    private final HttpServletRequest request;

    private String useCase;

    public DroneApiController(DroneFactory droneFactory, HttpServletRequest request) {
        this.droneFactory = droneFactory;
        this.request = request;
    }

    @GetMapping
    public ResponseEntity getAllDrones() {
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body("Use case: " + useCase + " not Implemented..!");

        List<DroneDTO> drones = droneService.getAllDrones();
        if (drones == null || drones.isEmpty())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No Drones Found..!");

        return ResponseEntity.ok().body(drones);
    }

    @PostMapping()
    public ResponseEntity registerDrone(@Valid @RequestBody DroneDTO droneDTO){
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
        try {
            DroneDTO drone = droneService.registerDrone(droneDTO);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping({"/{serialNumber}"})
    public ResponseEntity loadDroneWithMedications(@PathVariable String serialNumber, @Valid @RequestBody List<MedicationDTO> medicationItems) throws DroneExceptions {
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
        try {
            droneService.loadDroneWithMedications(serialNumber, medicationItems);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (DroneExceptions e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping({"/{serialNumber}"})
    public ResponseEntity getMedicationsDrone(@PathVariable String serialNumber) {
        DroneDTO drone;
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body("Use case: " + useCase + " not Implemented..!");
        try {
            drone = droneService.checkLoadedMedicationsForDrone(serialNumber);
        } catch (DroneExceptions e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok().body(drone);
    }

    @GetMapping({"battery/{serialNumber}"})
    public ResponseEntity checkDroneBattery(@PathVariable String serialNumber) {
        Integer battery=0;
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body("Use case: " + useCase + " not Implemented..!");
        try {
            battery = droneService.checkDroneBattery(serialNumber);
        } catch (DroneExceptions e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok().body(battery+"%");
    }

    @GetMapping({"audit/{serialNumber}"})
    public ResponseEntity checkDroneHistory(@PathVariable String serialNumber) {
        List<DroneHistoryDTO> droneHistoryDTOS;
        useCase = request.getHeader("use-case");
        IDroneService droneService = droneFactory.getClient(useCase);
        if (droneService == null)
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body("Use case: " + useCase + " not Implemented..!");
        try {
            droneHistoryDTOS = droneService.checkDroneHistory(serialNumber);
        } catch (DroneExceptions e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok().body(droneHistoryDTOS);
    }


}
