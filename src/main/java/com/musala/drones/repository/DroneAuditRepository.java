package com.musala.drones.repository;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.audit.DroneHistory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneAuditRepository extends JpaRepository<DroneHistory, String> {

    List<DroneHistory> findBySerialNumber(String serialNumber);
}
