package com.musala.drones.exception;

public class DroneExceptions extends Throwable {
    public DroneExceptions(String message) {
        super(message);
    }

    public static class CreateDroneExceptions extends DroneExceptions {
        public CreateDroneExceptions(String message) {
            super(message);
        }
    }

    public static class DroneNotFoundException extends DroneExceptions {
        String drone;
        public DroneNotFoundException(String message) {
            super(message);
        }
    }

    public static class DroneNotValidException extends DroneExceptions {
        String drone;
        public DroneNotValidException(String message) {
            super(message);
        }
    }
    public static class DroneExceedLimitException extends DroneExceptions {
        public DroneExceedLimitException(String message) {
            super(message);
        }
    }

}
