package com.musala.drones.service;

import com.musala.drones.MockReadUtil;
import com.musala.drones.domain.Drone;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.exception.DroneExceptions;
import com.musala.drones.repository.DroneRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class DroneServiceImpTest {

    @Mock
    private DroneRepository droneRepository;

    @InjectMocks
    private DroneServiceImp droneServiceImp;

    private List<Drone> droneList;

    private List<DroneDTO> droneDTOList;

    private DroneDTO droneDTO;

    private Drone drone;


    @Test
    @DisplayName("List All Drones || Happy Scenario")
    void getAllDrones_success() throws IOException {
        droneList=(List<Drone>) MockReadUtil
                .readListFromJsonFile("classpath:getAllDrones-response.json",
                        Drone.class);
        when(droneRepository.findAll()).thenReturn(droneList);
        droneDTOList = droneServiceImp.getAllDrones();
        Assert.notNull(droneDTOList);
    }

    @Test
    @DisplayName("Register Drone || Drone Already Exist")
    void registerDrone_droneAlreadyExist() throws IOException, DroneExceptions.CreateDroneExceptions {
        droneDTO=(DroneDTO) MockReadUtil.readObjectFromJsonFile("classpath:register-drone-request.json",DroneDTO.class);
        drone=(Drone)  MockReadUtil.readObjectFromJsonFile("classpath:drone.json",Drone.class);
        Optional<Drone>  droneOptional=Optional.of(drone);
        when(droneRepository.findById(any())).thenReturn(droneOptional);
        assertThrows(DroneExceptions.CreateDroneExceptions.class, () -> {
            droneServiceImp.registerDrone(droneDTO);
    });

    }

    @Test
    @DisplayName("Load Drone Medication for non exist drone || Drone Already Exist")
    void loadDroneWithMedications_failed_noDroneFound() throws IOException {
        drone=(Drone)  MockReadUtil.readObjectFromJsonFile("classpath:drone.json",Drone.class);
        Optional<Drone>  droneOptional=Optional.empty();
        when(droneRepository.findById(any())).thenReturn(droneOptional);
        assertThrows(DroneExceptions.DroneNotFoundException.class, () -> {
            droneServiceImp.loadDroneWithMedications("454121",new ArrayList<>());
        });
    }

    @Test
    @DisplayName("Load Drone Medication  || Drone Already Loaded ")
    void loadDroneWithMedications_failed_droneAlreadyLoaded() throws IOException {
        drone=(Drone)  MockReadUtil.readObjectFromJsonFile("classpath:drone.json",Drone.class);
        List<MedicationDTO> medicationDTOList=MockReadUtil.readListFromJsonFile("classpath:medications.json",MedicationDTO.class);
        Optional<Drone>  droneOptional=Optional.of(drone);
        when(droneRepository.findById(any())).thenReturn(droneOptional);
        assertThrows(DroneExceptions.DroneNotValidException.class, () -> {
            droneServiceImp.loadDroneWithMedications("454121",medicationDTOList);
        });
    }
}